#include "utility.hpp"

#include <cmath>

namespace jk::cache_assistant::utility {
double construct_from_parts(signed int integral, unsigned int decimal) {
  unsigned int decimal_places =
      (decimal == 0 ? 0 : std::log10<unsigned int>(decimal)) +
      1; // For 0 we can say any number of decimal places. It does not matter.
  return integral +
         std::copysign(decimal, integral) / std::pow(10, decimal_places);
}
} // namespace jk::cache_assistant::utility
