#ifndef COORD_EXPRESSION_PARSER_H
#define COORD_EXPRESSION_PARSER_H

#include "coord_expression.hpp"
#include <boost/spirit/include/qi.hpp>

BOOST_FUSION_ADAPT_STRUCT(jk::cache_assistant::coord_expression::placeholder,
                          (char, name))
BOOST_FUSION_ADAPT_STRUCT(jk::cache_assistant::coord_expression::digit,
                          (uint8_t, val))

BOOST_FUSION_ADAPT_STRUCT(jk::cache_assistant::coord_expression::number, digits)
BOOST_FUSION_ADAPT_STRUCT(jk::cache_assistant::coord_expression::decimal,
                          integer, fractional)
BOOST_FUSION_ADAPT_STRUCT(jk::cache_assistant::coord_expression::coordinate,
                          lat_dir, lat, lon_dir, lon)

namespace jk::cache_assistant::coord_expression::parser {

template <typename Iterator>
struct placeholder_parser
    : boost::spirit::qi::grammar<Iterator, placeholder()> {
  placeholder_parser() : placeholder_parser::base_type(start) {
    start %= boost::spirit::ascii::char_('A', 'Z');
  }

  boost::spirit::qi::rule<Iterator, placeholder()> start;
};

template <typename Iterator>
struct digit_parser : boost::spirit::qi::grammar<Iterator, digit()> {
  digit_parser() : digit_parser::base_type(start) {
    start %= boost::spirit::int_;
  }

  boost::spirit::qi::rule<Iterator, digit()> start;
};

template <typename Iterator>
struct number_element_parser
    : boost::spirit::qi::grammar<Iterator, std::variant<digit, placeholder>()> {
  number_element_parser() : number_element_parser::base_type(start) {
    start %= (digpar | placepar);
  }

  digit_parser<Iterator> digpar;
  placeholder_parser<Iterator> placepar;
  boost::spirit::qi::rule<Iterator, std::variant<digit, placeholder>()> start;
};

template <typename Iterator>
struct number_parser : boost::spirit::qi::grammar<Iterator, number()> {

  number_element_parser<Iterator> nep;
  number_parser() : number_parser::base_type(start) {
    rule %= +nep;
    start %= rule >> boost::spirit::qi::eps;
  }

  boost::spirit::qi::rule<Iterator,
                          std::vector<std::variant<digit, placeholder>>()>
      rule;
  boost::spirit::qi::rule<Iterator, number()> start;
};

template <typename Iterator>
struct LON_parser : boost::spirit::qi::symbols<char, LON> {
  LON_parser() {
    add("E", LON::E);
    add("W", LON::W);
  }
};

template <typename Iterator>
struct LAT_parser : boost::spirit::qi::symbols<char, LAT> {
  LAT_parser() {
    add("N", LAT::N);
    add("S", LAT::S);
  }
};

template <typename Iterator>
struct coordinate_parser : boost::spirit::qi::grammar<Iterator, coordinate()> {
  coordinate_parser() : coordinate_parser::base_type(start) {
    decimal = nump >> "." >> nump;
    start %=
      (latp >> " " >> decimal >> "," >> lonp >> " " >> decimal); // TODO order of elements.
  }

  number_parser<Iterator> nump;
  LON_parser<Iterator> lonp;
  LAT_parser<Iterator> latp;
  boost::spirit::qi::rule<Iterator, decimal()> decimal;
  boost::spirit::qi::rule<Iterator, coordinate()> start;
};

} // namespace jk::cache_assistant::coord_expression::parser

#endif /* COORD_EXPRESSION_PARSER_H */
