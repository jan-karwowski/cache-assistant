#ifndef COORD_EXPRESSION_H
#define COORD_EXPRESSION_H

#include "utility.hpp"
#include <cmath>
#include <cstdint>
#include <exception>
#include <functional>
#include <map>
#include <memory>
#include <numeric>
#include <set>
#include <sstream>
#include <string>
#include <variant>
#include <vector>

namespace jk::cache_assistant::coord_expression {

struct digit {
  digit() : val(-1) {}
  digit(digit &&) = default;
  digit &operator=(const digit &) = default;
  digit &operator=(digit &&) = default;
  digit(uint8_t val) : val(val) {}
  digit(const digit &) = default;

  friend bool operator==(const digit &, const digit &) = default;
  uint8_t val;
};

/// Note: it may contain one or more digits!
struct placeholder {
  placeholder(char name) : name(name) {}
  placeholder() : name(0) {}
  placeholder(const placeholder &) = default;
  placeholder &operator=(const placeholder &) = default;
  friend bool operator==(const placeholder &, const placeholder &) = default;
  char name;
};

struct number {
  number() {}
  number(const number &) = default;
  number(number &&) = default;
  number &operator=(const number &) = default;
  number &operator=(number &&) = default;
  number &operator=(const std::vector<std::variant<digit, placeholder>> &vec) {
    digits = vec;
    return *this;
  }
  number(std::vector<std::variant<digit, placeholder>> &&digits)
      : digits(std::move(digits)) {}
  number(const std::vector<std::variant<digit, placeholder>> &digits)
      : digits(digits) {}

  std::vector<std::variant<digit, placeholder>> digits;

  friend bool operator==(const number &, const number &) = default;
};

struct decimal {
  number integer;
  number fractional;

  friend bool operator==(const decimal &, const decimal &) = default;
};

enum struct LON { E, W };
enum struct LAT { N, S };

struct coordinate {
  LON lon_dir;
  decimal lon;
  LAT lat_dir;
  decimal lat;

  friend bool operator==(const coordinate &, const coordinate &) = default;
};

template <class Op> struct binary_op;

typedef std::variant<number, binary_op<std::plus<int>>> numeric_expression;

template <class Op> struct binary_op {
  std::unique_ptr<numeric_expression> left;
  std::unique_ptr<numeric_expression> right;
};

struct wildcard_coortinate {
  numeric_expression lon;
  numeric_expression lat;
};

namespace placeholder_values {
struct known_value {
  int val;

  inline std::vector<int> all_valuations() const { return {val}; }
};

struct known_range {
  int min_incl;
  int max_incl;

  inline std::vector<int> all_valuations() const {
    std::vector<int> ret(max_incl - min_incl + 1);
    std::iota(ret.begin(), ret.end(), min_incl);
    return ret;
  }
};

struct possible_values {
  std::set<int> values;

  inline const std::vector<int> all_valuations() const {
    return std::vector<int>(values.begin(), values.end());
  }
};

typedef std::variant<known_value, known_range, possible_values> value;
} // namespace placeholder_values

struct undefined_placeholder_exception : std::exception {
  char name;
  std::string message;
  undefined_placeholder_exception(char name) : name(name) {
    std::stringstream ss;
    ss << "A value for placeholder " << message << " was not defined.";
    message = ss.str();
  }

  const char *what() const noexcept override { return message.c_str(); }
};

template <class Node> struct expression_calculator {
  std::vector<int>
  all_valuations(const Node &,
                 const std::map<char, placeholder_values::value> &) = delete;
};

template <> struct expression_calculator<digit> {
  std::vector<int>
  all_valuations(const digit &d,
                 const std::map<char, placeholder_values::value> &) {
    return {d.val};
  }
};

template <> struct expression_calculator<placeholder> {
  std::vector<int>
  all_valuations(const placeholder &p,
                 const std::map<char, placeholder_values::value> &vals) {
    auto it = vals.find(p.name);
    if (it == vals.end()) {
      throw undefined_placeholder_exception(p.name);
    } else {
      return std::visit([](auto &&value) { return value.all_valuations(); },
                        it->second);
    }
  }
};

template <> struct expression_calculator<number> {
  std::vector<int>
  all_valuations(const number &exp,
                 const std::map<char, placeholder_values::value> &vals) const {
    std::vector<int> ret{0};
    for (auto &&el : exp.digits) {
      std::vector<int> tails = std::visit(
          [vals](auto &v) {
            return expression_calculator<std::decay_t<decltype(v)>>()
                .all_valuations(v, vals);
          },
          el);
      std::vector<int> appended;
      appended.reserve(ret.size() * tails.size());
      for (int head : ret) {
        for (int tail : tails) {
          int exponent = std::max<int>(1, std::log10(tail));
          appended.push_back(head * std::pow(10, exponent) + tail);
        }
      }
      ret = std::move(appended);
    }

    return ret;
  }
};

template <> struct expression_calculator<decimal> {
  std::vector<double>
  all_valuations(const decimal &exp,
                 const std::map<char, placeholder_values::value> &vals) const {
    expression_calculator<number> calc;
    auto frac = calc.all_valuations(exp.fractional, vals);
    auto intg = calc.all_valuations(exp.integer, vals);
    std::vector<double> ret;
    ret.reserve(frac.size() * intg.size());
    for (auto integer : intg) {
      for (auto fraction : frac) {
        ret.push_back(utility::construct_from_parts(integer, fraction));
      }
    }
    return ret;
  }
};

template <class Op> struct expression_calculator<binary_op<Op>> {
  std::vector<int>
  all_valuations(const binary_op<Op> &binop,
                 const std::map<char, placeholder_values::value> &vals) {
    return std::visit(
        [&vals](auto &&l, auto &&r) {
          std::vector<int> ret;
          Op op;
          for (auto lval : expression_calculator<std::decay_t<decltype(l)>>()
                               .all_valuations(l, vals)) {
            for (auto rval : expression_calculator<std::decay_t<decltype(r)>>()
                                 .all_valuations(r, vals)) {
              ret.push_back(op(lval, rval));
            }
          }
          return ret;
        },
        *binop.left, *binop.right);
  }
};

template <> struct expression_calculator<numeric_expression> {
  std::vector<int>
  all_valuations(const numeric_expression &exp,
                 const std::map<char, placeholder_values::value> &vals) const {
    return std::visit(
        [&vals](auto &&v) {
          return expression_calculator<std::decay_t<decltype(v)>>()
              .all_valuations(v, vals);
        },
        exp);
  }
};

inline std::ostream &operator<<(std::ostream &os, const placeholder &ph) {
  os << "PLACEHOLDER(" << ph.name << ")";
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const digit &dig) {
  os << "DIGIT(" << (int)dig.val << ")";
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const number &num) {
  os << "[";
  for (const auto &val : num.digits) {
    std::visit([&os](auto &&v) { os << v; }, val);
  }
  os << "]";
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const LON &lon) {
  switch (lon) {
  case LON::E:
    os << 'E';
    break;
  case LON::W:
    os << 'W';
    break;
  default:
    os << '?';
    break;
  }
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const LAT &lat) {
  switch (lat) {
  case LAT::N:
    os << 'N';
  case LAT::S:
    os << 'S';
    break;
  default:
    os << '?';
  }
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const decimal &dec) {
  os << "DECIMAL(" << dec.integer << "," << dec.fractional << ")";
  return os;
}

inline std::ostream &operator<<(std::ostream &os, const coordinate &coord) {
  os << "COORD(" << coord.lat_dir << coord.lat << coord.lon_dir << coord.lon
     << ")";
  return os;
}

} // namespace jk::cache_assistant::coord_expression

#endif /* COORD_EXPRESSION_H */
