#ifndef GPX_HELPERS_H
#define GPX_HELPERS_H

#include <filesystem>
#include <vector>

namespace jk::cache_assistant::gpx_helpers {

struct coord {
  double lon;
  double lat;
};

void coords_to_gdal_dataset(const std::filesystem::path& path, const std::vector<coord> &coords);

} // namespace jk::cache_assistant::gpx_helpers

#endif /* GPX_HELPERS_H */
