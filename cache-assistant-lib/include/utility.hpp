#ifndef UTILITY_H
#define UTILITY_H
namespace jk::cache_assistant::utility {
double construct_from_parts(signed int integral, unsigned int decimal);
}

#endif /* UTILITY_H */
