#include "gpx_helpers.hpp"

#include <cassert>
#include "ogrsf_frmts.h"

namespace jk::cache_assistant::gpx_helpers {

void coords_to_gdal_dataset(const std::filesystem::path &path,
                                            const std::vector<coord> &coords) {
  GDALAllRegister();
  GDALDriver *gpx_driver = GDALDriver::FromHandle(GDALGetDriverByName("GPX"));

  GDALDatasetUniquePtr dataset(
      gpx_driver->Create(path.c_str(), 0, 0, 0, GDT_Unknown, NULL));
  OGRLayer* layer(dataset->CreateLayer("layer", nullptr, wkbPoint, nullptr));

  for (auto &&point : coords) {
    OGRFeatureUniquePtr feature(
				OGRFeature::CreateFeature(layer->GetLayerDefn()));
    feature->SetField("Name", "point");
    OGRPoint pt;
    pt.setX(point.lon);
    pt.setY(point.lat);
    feature->SetGeometry(&pt);
    if (auto err = layer->CreateFeature(feature.get());
        err != OGRERR_NONE) {
      assert(false); // TODO
    }
  }
}

} // namespace jk::cache_assistant::gpx_helpers
