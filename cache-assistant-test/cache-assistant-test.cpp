#define BOOST_TEST_MODULE lib_test

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <filesystem>
#include <fstream>
#include <sstream>

#include "coord_expression.hpp"
#include "coord_expression_parser.hpp"
#include "gpx_helpers.hpp"

namespace ce = jk::cache_assistant::coord_expression;

namespace std {
template <class... Args>
ostream &operator<<(ostream &out, const std::variant<Args...> &var) {
  std::visit([&out](auto &&x) { out << "VARIANT(" << x << ")"; }, var);

  return out;
}
} // namespace std

BOOST_AUTO_TEST_SUITE(GpxWriteTest)

BOOST_AUTO_TEST_CASE(SimpleGpxWrite) {
  std::vector<jk::cache_assistant::gpx_helpers::coord> coords{{21, 54},
                                                              {22, 51}};
  std::filesystem::path path("test.gpx");
  std::filesystem::remove(path);
  jk::cache_assistant::gpx_helpers::coords_to_gdal_dataset(path, coords);
  std::string expected_gpx =
      "<?xml version=\"1.0\"?>\n<gpx version=\"1.1\" creator=\"GDAL 3.6.2\" "
      "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
      "xmlns=\"http://www.topografix.com/GPX/1/1\" "
      "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 "
      "http://www.topografix.com/GPX/1/1/gpx.xsd\">\n<metadata><bounds "
      "minlat=\"51.000000000000000\" minlon=\"21.000000000000000\" "
      "maxlat=\"54.000000000000000\" "
      "maxlon=\"22.000000000000000\"/></metadata>                  \n<wpt "
      "lat=\"54.0\" lon=\"21.0\">\n  <name>point</name>\n</wpt>\n<wpt "
      "lat=\"51.0\" lon=\"22.0\">\n  <name>point</name>\n</wpt>\n</gpx>\n";
  std::ifstream stream(path);
  char buff[2048] = {0};
  stream.read(buff, sizeof(buff));
  std::string generated;
  generated.append(buff, stream.gcount());
  BOOST_TEST(generated == expected_gpx);
  std::filesystem::remove(path);
}

BOOST_AUTO_TEST_CASE(ExpressionCalculate) {
  ce::numeric_expression expr =
      ce::number({ce::digit{3}, ce::placeholder{'a'}, ce::placeholder{'b'}});

  std::map<char,
           jk::cache_assistant::coord_expression::placeholder_values::value>
      valuation{std::make_pair(
                    'a', ce::placeholder_values::possible_values{{3, 4, 5}}),
                std::make_pair('b', ce::placeholder_values::known_range{1, 5})};
  std::vector<int> valuations =
      ce::expression_calculator<ce::numeric_expression>().all_valuations(
          expr, valuation);

  std::sort(valuations.begin(), valuations.end());

  std::vector<int> expected_valuations{331, 332, 333, 334, 335, 341, 342, 343,
                                       344, 345, 351, 352, 353, 354, 355};

  BOOST_REQUIRE_EQUAL_COLLECTIONS(valuations.begin(), valuations.end(),
                                  expected_valuations.begin(),
                                  expected_valuations.end());
}

BOOST_AUTO_TEST_CASE(SumExpressionCalculate) {
  std::unique_ptr<ce::numeric_expression> left(
      new ce::numeric_expression(ce::number{{ce::placeholder{'a'}}}));
  std::unique_ptr<ce::numeric_expression> right(
      new ce::numeric_expression(ce::number{{ce::placeholder{'b'}}}));
  ce::numeric_expression expr = ce::binary_op<std::plus<int>>{
      .left = std::move(left), .right = std::move(right)};
  std::map<char,
           jk::cache_assistant::coord_expression::placeholder_values::value>
      valuation{
          std::make_pair('a', ce::placeholder_values::possible_values{{1, 9}}),
          std::make_pair('b', ce::placeholder_values::known_range{1, 5})};
  std::vector<int> valuations =
      ce::expression_calculator<ce::numeric_expression>().all_valuations(
          expr, valuation);

  std::vector<int> expected_valuations{2, 3, 4, 5, 6, 10, 11, 12, 13, 14};

  BOOST_REQUIRE_EQUAL_COLLECTIONS(valuations.begin(), valuations.end(),
                                  expected_valuations.begin(),
                                  expected_valuations.end());
}

BOOST_AUTO_TEST_CASE(DigitParserTest) {
  std::string input = "12";
  jk::cache_assistant::coord_expression::digit expected_output(12);
  jk::cache_assistant::coord_expression::parser::digit_parser<
      decltype(input)::iterator>
      parser;
  jk::cache_assistant::coord_expression::digit calculated_output;
  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_CHECK(parsed);
  BOOST_CHECK(begin == input.end());
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_CASE(PlaceholderParserTest) {
  std::string input = "B";
  jk::cache_assistant::coord_expression::placeholder expected_output{'B'};
  jk::cache_assistant::coord_expression::parser::placeholder_parser<
      decltype(input)::iterator>
      parser;
  jk::cache_assistant::coord_expression::placeholder calculated_output;
  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_CHECK(parsed);
  BOOST_CHECK(begin == input.end());
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_CASE(NumberElementParserTest) {
  std::string input = "A";
  std::variant<jk::cache_assistant::coord_expression::digit,
               jk::cache_assistant::coord_expression::placeholder>
      expected_output = jk::cache_assistant::coord_expression::placeholder{'A'};

  jk::cache_assistant::coord_expression::parser::number_element_parser<
      decltype(input)::iterator>
      parser;

  std::variant<jk::cache_assistant::coord_expression::digit,
               jk::cache_assistant::coord_expression::placeholder>
      calculated_output;

  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_CHECK(parsed);
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_CASE(NumberElementParserTest2) {
  std::string input = "14";
  std::variant<jk::cache_assistant::coord_expression::digit,
               jk::cache_assistant::coord_expression::placeholder>
      expected_output = jk::cache_assistant::coord_expression::digit{14};

  jk::cache_assistant::coord_expression::parser::number_element_parser<
      decltype(input)::iterator>
      parser;

  std::variant<jk::cache_assistant::coord_expression::digit,
               jk::cache_assistant::coord_expression::placeholder>
      calculated_output;

  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_CHECK(parsed);
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_CASE(NumberParserTest1) {
  std::string input = "14A2";
  jk::cache_assistant::coord_expression::number expected_output{
      {jk::cache_assistant::coord_expression::digit{14},
       jk::cache_assistant::coord_expression::placeholder{'A'},
       jk::cache_assistant::coord_expression::digit{2}}};

  jk::cache_assistant::coord_expression::parser::number_parser<
      decltype(input)::iterator>
      parser;

  jk::cache_assistant::coord_expression::number calculated_output;

  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_CHECK(parsed);
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_CASE(DoubleFromParts1) {
  BOOST_CHECK_EQUAL(2.5,
                    jk::cache_assistant::utility::construct_from_parts(2, 5));
  BOOST_CHECK_EQUAL(2.25,
                    jk::cache_assistant::utility::construct_from_parts(2, 25));
  BOOST_CHECK_EQUAL(0.5,
                    jk::cache_assistant::utility::construct_from_parts(0, 5));
  BOOST_CHECK_EQUAL(-2.5,
                    jk::cache_assistant::utility::construct_from_parts(-2, 5));
}

BOOST_AUTO_TEST_CASE(COORDParserTest1) {
  std::string input = "N 14A2.BC,W 14A2.BC";
  jk::cache_assistant::coord_expression::decimal coord{
      {{jk::cache_assistant::coord_expression::digit{14},
        jk::cache_assistant::coord_expression::placeholder{'A'},
        jk::cache_assistant::coord_expression::digit{2}}},
      {{jk::cache_assistant::coord_expression::placeholder{'B'},
        jk::cache_assistant::coord_expression::placeholder{'C'}}}};
  jk::cache_assistant::coord_expression::coordinate expected_output{
      jk::cache_assistant::coord_expression::LON::W, coord,
      jk::cache_assistant::coord_expression::LAT::N, coord};

  jk::cache_assistant::coord_expression::parser::coordinate_parser<
      decltype(input)::iterator>
      parser;

  jk::cache_assistant::coord_expression::coordinate calculated_output;

  auto begin = input.begin();
  bool parsed = boost::spirit::qi::phrase_parse(
      begin, input.end(), parser, boost::spirit::qi::space, calculated_output);
  BOOST_TEST_CHECKPOINT("Parsed characters count " << (begin - input.begin()));
  BOOST_CHECK(parsed);
  BOOST_CHECK_EQUAL(calculated_output, expected_output);
}

BOOST_AUTO_TEST_SUITE_END()
